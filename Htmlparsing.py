#!/usr/bin/python

import openpyxl 
wb = openpyxl.load_workbook(filename='PMaths_TBTM_20190627.xlsx', data_only=True)               #file name
sheet = wb.worksheets[0]                                                                        #fixed to be the 1st sheet in the workbook

for i in range(2, sheet.max_row, 1):                                                            #from the 2nd row to the last row
    if sheet.cell(row=i,column=14).value != "" and sheet.cell(row=i,column=18).value != "":
        a = str(sheet.cell(row=i,column=14).value)                                              #the 1st col to be considered
        b = sheet.cell(row=i,column=18).value                                                   #the 2st col to be considered
        if "https://eresources.oupchina.com.hk/ME/opm/links/" in a:                             #the value in the 1st col to be considered must include this string, otherwise no action will be taken
            c = a.lstrip("https://eresources.oupchina.com.hk/ME/opm/links/")                    #extract the file name, extension ".html" should already be included
            f = open(c,"a")
            f.writelines('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n')
            f.writelines('<html xmlns="http://www.w3.org/1999/xhtml">\n')
            f.writelines('<head>\n')
            f.writelines('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n')
            f.writelines('<meta http-equiv="refresh" content="0;url=%s">\n' % (b))
            f.writelines('<title>Oxford University Press</title>\n')
            f.writelines('</head>\n')
            f.writelines('<body>\n')
            f.writelines('</body>\n')
            f.writelines('</html>\n')
            f.close